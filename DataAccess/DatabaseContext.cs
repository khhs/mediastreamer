﻿using Model;
using System.Data.Entity;

namespace DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DbSet<MediaSong> Songs { get; set; }
        public DbSet<MediaImage> Images { get; set; }
        public DbSet<MediaVideo> Videos { get; set; }

        public DatabaseContext()
            : base(@"Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=")
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }
    }
}
