﻿using System;

namespace Model
{
    public class MediaImage
    {
        public int Id { get; set; }
        public String ImageName { get; set; }
        public String FileType { get; set; }
        public String ImagePath { get; set; }

        public MediaImage() { }

        public MediaImage(int id, String imageName, String fileType)
        {
            Id = id;
            ImageName = imageName;
            FileType = fileType;
        }

        public MediaImage(String imageName, String fileType)
        {
            ImageName = imageName;
            FileType = fileType;
        }
    }
}
