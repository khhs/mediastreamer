﻿using System;

namespace Model
{
    public class MediaVideo
    {
        public int Id { get; set; }
        public String VideoName { get; set; }
        public String FileType { get; set; }
        public String VideoPath { get; set; }

        public MediaVideo() { }

        public MediaVideo(int id, String videoName, String fileType)
        {
            Id = id;
            VideoName = videoName;
            FileType = fileType;
        }

        public MediaVideo(String videoName, String fileType)
        {
            VideoName = videoName;
            FileType = fileType;
        }
    }
}
