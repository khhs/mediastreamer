﻿using System;

namespace Model
{ 
    public class MediaSong
    {
        public int Id { get; set; }
        public String SongName { get; set; }
        public String FileType {get; set;}
        public String Artist { get; set; }
        public String Album { get; set; }
        public String SongLength { get; set; }
        public String SongPath { get; set; }

        public MediaSong() { }

        public MediaSong(int id, String songName, String fileType, String artist, String album, String songLength)
        {
            Id = id;
            SongName = songName;
            FileType = fileType;
            Artist = artist;
            Album = album;
            SongLength = songLength;
        }

        public MediaSong(String songName, String fileType, String artist, String album, String songLength)
        {
            SongName = songName;
            FileType = fileType;
            Artist = artist;
            Album = album;
            SongLength = songLength;
        }
    }
}
