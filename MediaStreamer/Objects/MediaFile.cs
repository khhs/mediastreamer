﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace MediaStreamer.Objects
{
    public class MediaFile
    {
        private ReadOnlyCollection<Byte> PrivateFileBytes;
        public ReadOnlyCollection<Byte> FileBytes { get { return PrivateFileBytes; } }

        public StorageFile File { get; set; }

        public MediaFile(StorageFile file)
        {
            File = file;
        }

        public async Task SetupUploadBuffer()
        {
            IRandomAccessStreamWithContentType Stream = await File.OpenReadAsync();
            Byte[] FileBuffer = new Byte[Stream.Size];
            DataReader Reader = new DataReader(Stream);
            await Reader.LoadAsync((uint)Stream.Size);
            Reader.ReadBytes(FileBuffer);
            PrivateFileBytes = new ReadOnlyCollection<Byte>(FileBuffer);
        }
    }
}
