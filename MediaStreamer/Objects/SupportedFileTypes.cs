﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaStreamer.Objects
{
    class SupportedFileTypes
    {
        public List<String> ImageFileTypes { get { return new List<String>() { ".jpg", ".jpeg", ".png" }; } }
        public List<String> MusicFileTypes { get { return new List<String>() { ".mp3", ".m4a", ".wma" }; } }
        public List<String> VideoFileTypes { get { return new List<String>(){ ".avi", ".wmv", ".mp4" }; } }
    }
}
