﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Windows.Storage;

namespace MediaStreamer
{
    class ServerCredential : NetworkCredential
    {

        public ServerCredential(){
            this.UserName = "";
            this.Password = "";
        }

        public String MusicFilesPath { get { return "http://khhs.net/MediaStreamer/music/"; } }
        public String ImageFilesPath { get { return "http://khhs.net/MediaStreamer/image/"; } }
        public String VideoFilesPath { get { return "http://khhs.net/MediaStreamer/video/"; } }

        public String MusicServerFolderPath { get { return "ftp://" + UserName + "@web7.gigahost.dk/www/khhs.net/MediaStreamer/music/"; } }
        public String ImageServerFolderPath { get { return "ftp://" + UserName + "@web7.gigahost.dk/www/khhs.net/MediaStreamer/image/"; } }
        public String VideoServerFolderPath { get { return "ftp://" + UserName + "@web7.gigahost.dk/www/khhs.net/MediaStreamer/video/"; } }
    }
}
