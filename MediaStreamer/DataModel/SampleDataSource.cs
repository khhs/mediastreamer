﻿using Model;
using System;
using System.Collections.ObjectModel;

namespace MediaStreamer.Data
{
    public sealed class SampleDataSource
    {
        public ObservableCollection<MediaImage> ImageItems { get; private set; }
        public ObservableCollection<MediaSong> SongItems { get; private set; }
        public ObservableCollection<MediaVideo> VideoItems { get; private set; }


    }
}
