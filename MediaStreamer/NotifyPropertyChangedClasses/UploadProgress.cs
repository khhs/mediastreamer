﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace MediaStreamer.NotifyPropertyChangedClasses
{
    // Hentet fra http://stackoverflow.com/questions/1315621/implementing-inotifypropertychanged-does-a-better-way-exist
    public class UploadProgress : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        protected Boolean SetField<T>(ref T field, T value, String propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        private long Progress;
        public long UploaderProgress
        {
            get { return Progress; }
            set { SetField(ref Progress, value, "UploaderProgress"); }
        }

        public double TimeStarted { get; set; }
    }
}
