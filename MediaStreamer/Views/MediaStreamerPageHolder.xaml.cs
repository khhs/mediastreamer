﻿using MediaStreamer.NotifyPropertyChangedClasses;
using MediaStreamer.Objects;
using MediaStreamer.Views;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MediaStreamer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MediaStreamerPageHolder : Page
    {
        public MediaElement MediaPlayer { get { return MusicMediaPlayer; } }
        public HttpClient ServiceClient {get; private set;}
        public Uri ServiceUrl { get { return new Uri("http://localhost:29042/api/"); } }
        public Boolean IsShuffling {get ; set;}
        public Boolean IsLooping { get; set; }
        public UploadProgress NotifyUploadProgress { get; set; }
        public MediaFile FileToUploadHolder { get; set; }

        public MediaStreamerPageHolder()
        {
            this.InitializeComponent();
            SetupServiceClient();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            LocalFrame.Navigate(typeof(MusicPage));
        }

        /// <summary>
        /// Setups the service client.
        /// </summary>
        private void SetupServiceClient()
        {
            ServiceClient = new HttpClient();
            ServiceClient.BaseAddress = ServiceUrl;
            ServiceClient.DefaultRequestHeaders.Clear();
            ServiceClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Sets the BTN colors.
        /// </summary>
        /// <param name="btnClicked">The BTN clicked.</param>
        private void SetBtnColors(Button btnClicked)
        {
            SolidColorBrush ColorBrush = new SolidColorBrush(Colors.DarkGray);
            BtnUpload.Foreground = ColorBrush;
            BtnMusic.Foreground = ColorBrush;
            BtnImage.Foreground = ColorBrush;
            BtnVideo.Foreground = ColorBrush;
            btnClicked.Foreground = new SolidColorBrush(Colors.White);
        }

        /// <summary>
        /// Handles the Click event of the BtnUpload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BtnUpload_Click(object sender, RoutedEventArgs e)
        {
            SetBtnColors((Button)sender);
            if (!LocalFrame.CurrentSourcePageType.Name.Equals("UploadPage"))
            {
                LocalFrame.Navigate(typeof(UploadPage));
            }
        }

        /// <summary>
        /// Handles the Click event of the BtnMusic control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BtnMusic_Click(object sender, RoutedEventArgs e)
        {
            SetBtnColors((Button)sender);
            if (!LocalFrame.CurrentSourcePageType.Name.Equals("MusicPage"))
            {
                LocalFrame.Navigate(typeof(MusicPage));
            }
        }

        /// <summary>
        /// Handles the Click event of the BtnImage control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BtnImage_Click(object sender, RoutedEventArgs e)
        {
            SetBtnColors((Button)sender);
            if (!LocalFrame.CurrentSourcePageType.Name.Equals("ImagePage"))
            {
                LocalFrame.Navigate(typeof(ImagePage));
            }
        }

        /// <summary>
        /// Handles the Click event of the BtnVideo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BtnVideo_Click(object sender, RoutedEventArgs e)
        {
            SetBtnColors((Button)sender);
            if (!LocalFrame.CurrentSourcePageType.Name.Equals("VideoPage"))
            {
                LocalFrame.Navigate(typeof(VideoPage));
            }
        }

        /// <summary>
        /// Shows the side menu.
        /// </summary>
        public void ShowSideMenu()
        {
            SideBarMenu.Width = 150;
            LocalFrame.Margin = new Thickness() { Left = 150 };
        }

        /// <summary>
        /// Hides the side menu.
        /// </summary>
        public void HideSideMenu()
        {
            SideBarMenu.Width = 0;
            LocalFrame.Margin = new Thickness(){Left = 0};
        }
    }
}
