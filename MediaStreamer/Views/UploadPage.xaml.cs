﻿using MediaStreamer.DataSourceClasses;
using MediaStreamer.FtpManagers;
using MediaStreamer.NotifyPropertyChangedClasses;
using MediaStreamer.Objects;
using Model;
using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Pickers;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace MediaStreamer.Views
{
    public sealed partial class UploadPage : Page
    {
        private MediaFile FileToUpload;
        private MusicDataSource MusDataSource = new MusicDataSource();
        private ImageDataSource ImgDataSource = new ImageDataSource();
        private VideoDataSource VidDataSource = new VideoDataSource();
        private MediaUploader MedUploader = new MediaUploader();
        private SupportedFileTypes SuppFileTypes = new SupportedFileTypes();
        private MediaStreamerPageHolder FramePageHolder = ((Frame)Window.Current.Content).Content as MediaStreamerPageHolder;

        /// <summary>
        /// Initializes a new instance of the <see cref="UploadPage"/> class.
        /// </summary>
        public UploadPage()
        {
            this.InitializeComponent();
            CheckUploaderForUpload();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (FramePageHolder.NotifyUploadProgress != null)
            {
                FramePageHolder.NotifyUploadProgress.PropertyChanged -= UpdateUploaderProgress;
            }
        }

        /// <summary>
        /// Sets the upload file.
        /// </summary>
        private async void SetUploadFile()
        {
            StorageFile FilePicked = await GetFileFromFilePicker();
            if (FilePicked == null)
                return;
            BtnUpload.IsEnabled = true;
            FileToUpload = new MediaFile(FilePicked);
            FramePageHolder.FileToUploadHolder = FileToUpload;
            SetupUploadGui();
        }

        /// <summary>
        /// Checks the uploader for upload.
        /// </summary>
        private void CheckUploaderForUpload()
        {
            if (FramePageHolder.NotifyUploadProgress != null)
            {
                FileToUpload = FramePageHolder.FileToUploadHolder;
                UploadingProgressGrid.Visibility = Visibility.Visible;
                BtnBrowse.IsEnabled = false;
                ResumeUploaderProgress();
            }
        }

        /// <summary>
        /// Resumes the uploader progress.
        /// </summary>
        private async void ResumeUploaderProgress()
        {
            UploadProgress UplProgress = FramePageHolder.NotifyUploadProgress;
            UplProgress.PropertyChanged += UpdateUploaderProgress;

            PrgBarUploader.Maximum = ((BasicProperties)await FileToUpload.File.GetBasicPropertiesAsync()).Size;
        }

        /// <summary>
        /// Gets the file from file picker.
        /// </summary>
        /// <returns></returns>
        private async Task<StorageFile> GetFileFromFilePicker()
        {
            FileOpenPicker FilePicker = new FileOpenPicker();
            FilePicker.ViewMode = PickerViewMode.List;
            FilePicker.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            AddFilterToFilePicker(FilePicker);
            return await FilePicker.PickSingleFileAsync();
        }

        /// <summary>
        /// Setups the upload GUI.
        /// </summary>
        private void SetupUploadGui()
        {
            ResetUploadGui();
            TxtFilePath.Text = FileToUpload.File.Path;
            TxtTitel.Text = FileToUpload.File.DisplayName;
            if (SuppFileTypes.MusicFileTypes.Contains(FileToUpload.File.FileType.ToLower()))
                SetMusicDetailTextInput(Visibility.Visible);
            TxtTitel.Visibility = Visibility.Visible;
            BtnUpload.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Resets the upload GUI.
        /// </summary>
        private void ResetUploadGui()
        {
            TxtTitel.Text = "";
            TxtArtist.Text = "";
            TxtAlbum.Text = "";
            TxtFilePath.Text = "";
        }

        /// <summary>
        /// Adds the filter to file picker.
        /// </summary>
        /// <param name="filePicker">The file picker.</param>
        private void AddFilterToFilePicker(FileOpenPicker filePicker)
        {
            foreach (String s in SuppFileTypes.ImageFileTypes)
                filePicker.FileTypeFilter.Add(s);
            foreach (String s in SuppFileTypes.VideoFileTypes)
                filePicker.FileTypeFilter.Add(s);
            foreach (String s in SuppFileTypes.MusicFileTypes)
                filePicker.FileTypeFilter.Add(s);
        }

        /// <summary>
        /// Sets the music detail text input visibility.
        /// </summary>
        /// <param name="vis">The vis.</param>
        private void SetMusicDetailTextInput(Visibility vis)
        {
            TxtArtist.Visibility = vis;
            TxtAlbum.Visibility = vis;
        }

        /// <summary>
        /// Handles the Click event of the BtnBrowse control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void BtnBrowse_Click(object sender, RoutedEventArgs e)
        {
            UploadingProgressGrid.Visibility = Visibility.Collapsed;
            SetMusicDetailTextInput(Visibility.Collapsed);
            GridFileProperties.Visibility = Visibility.Visible;
            SetUploadFile();
        }

        /// <summary>
        /// Handles the Click event of the BtnUpload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private async void BtnUpload_Click(object sender, RoutedEventArgs e)
        {
            if (await CheckIfFileExists())
            {
                if (InitiateSetupOfMediaFileUploading())
                {
                    PreparGuiForUpload();
                }
            }
        }

        /// <summary>
        /// Checks if file exists.
        /// </summary>
        /// <returns></returns>
        private async Task<Boolean> CheckIfFileExists(){
            try
            {
                if (await FileToUpload.File.OpenAsync(FileAccessMode.Read) == null)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Prepars the GUI for upload.
        /// </summary>
        private void PreparGuiForUpload() 
        {
            BtnUpload.IsEnabled = false;
            BtnBrowse.IsEnabled = false;
            UploadingProgressGrid.Visibility = Visibility.Visible;
            GridFileProperties.Visibility = Visibility.Collapsed;
        }

        //Denne metoden har en linje kode for mye, det kunne eventuelt av hver metode i if løkkene returnerte en boolean istede for å gjøre det under metoden
        /// <summary>
        /// Initiates the setup of media file uploading.
        /// </summary>
        /// <returns></returns>
        private Boolean InitiateSetupOfMediaFileUploading()
        {
            if (FileToUpload.File.ContentType.Contains("image"))
            {
                SetupUploadForMediaImage();
                return true;
            }
            else if (FileToUpload.File.ContentType.Contains("audio"))
            {
                SetupUploadForMediaSong();
                return true;
            }
            else if (FileToUpload.File.ContentType.Contains("video"))
            {
                SetupUploadForMediaVideo();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Initiates the uploader progress.
        /// </summary>
        /// <returns></returns>
        private async Task<UploadProgress> InitiateUploaderProgress()
        {
            UploadProgress NotifyUploadProgress = new UploadProgress();
            FramePageHolder.NotifyUploadProgress = NotifyUploadProgress;
            NotifyUploadProgress.PropertyChanged += UpdateUploaderProgress;
            NotifyUploadProgress.TimeStarted = DateTime.Now.TimeOfDay.TotalSeconds;

            PrgBarUploader.Maximum = ((BasicProperties) await FileToUpload.File.GetBasicPropertiesAsync()).Size;
            return NotifyUploadProgress;
        }

        /// <summary>
        /// Updates the uploader progress.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        private void UpdateUploaderProgress(object sender, object e)
        {
            UploadProgress UplProg = (UploadProgress)sender;
            long BytesUploaded = UplProg.UploaderProgress;
            SetUploadingTextInfo(BytesUploaded, UplProg.TimeStarted);
            PrgBarUploader.Value = BytesUploaded;
        }

        /// <summary>
        /// Sets the uploading text information.
        /// </summary>
        /// <param name="bytesUploaded">The bytes uploaded.</param>
        /// <param name="timeStarted">The time started.</param>
        private void SetUploadingTextInfo(long bytesUploaded, double timeStarted)
        {
            Decimal MegaBytesUploaded = ((Decimal)(bytesUploaded / 10000) / 100);
            Decimal FileSizeInMegaBytes = ((Decimal)(FileToUpload.FileBytes.Count / 10000) / 100);
            double UploadSpeedInKiloBytes = (bytesUploaded / (DateTime.Now.TimeOfDay.TotalSeconds - timeStarted)) / 1000;
            TxtUploadingInfo.Text = String.Format("{0}MB / {1}MB        {2}kB/s", MegaBytesUploaded, FileSizeInMegaBytes, (int)UploadSpeedInKiloBytes);
            if (MegaBytesUploaded == FileSizeInMegaBytes)
            {
                TxtUploadingInfo.Text = String.Format("{0}MB / {1}MB        {2}kB/s", MegaBytesUploaded, FileSizeInMegaBytes, 0);
                BtnBrowse.IsEnabled = true;
                PrgRingUploader.IsActive = false;
            }
        }

        /// <summary>
        /// Setups the upload for media image.
        /// </summary>
        private async void SetupUploadForMediaImage()
        {
            MediaImage MedImage = new MediaImage(TxtTitel.Text, FileToUpload.File.FileType);
            if (await ImgDataSource.AddImage(MedImage))
            {
                if (!await MedUploader.UploadMediaImage(MedImage, FileToUpload, await InitiateUploaderProgress()))
                    UploadingProgressGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                ShowInternetErrorMessage();
                UploadingProgressGrid.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Setups the upload for media song.
        /// </summary>
        private async void SetupUploadForMediaSong()
        {
            MusicProperties SongProperties = await FileToUpload.File.Properties.GetMusicPropertiesAsync();
            String SongTime = SongProperties.Duration.Minutes.ToString("00")+ ":"+SongProperties.Duration.Seconds.ToString("00");
            MediaSong MedSong = new MediaSong(TxtTitel.Text, FileToUpload.File.FileType, TxtArtist.Text, TxtAlbum.Text, SongTime);
            if (await MusDataSource.AddSong(MedSong))
            {
                if (!await MedUploader.UploadMediaSong(MedSong, FileToUpload, await InitiateUploaderProgress()))
                    UploadingProgressGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                ShowInternetErrorMessage();
                UploadingProgressGrid.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Setups the upload for media video.
        /// </summary>
        private async void SetupUploadForMediaVideo()
        {
            MediaVideo MedVideo = new MediaVideo(TxtTitel.Text, FileToUpload.File.FileType);
            if (await VidDataSource.AddVideo(MedVideo))
            {
                if (!await MedUploader.UploadMediaVideo(MedVideo, FileToUpload, await InitiateUploaderProgress()))
                    UploadingProgressGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                ShowInternetErrorMessage();
                UploadingProgressGrid.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Shows the internet error message. This method is explained in MusicPage
        /// </summary>
        private void ShowInternetErrorMessage()
        {
            try
            {
                MessageDialog MsgDialog = new MessageDialog("Får ikke kontakt med server, sjekk internet tilkobling");
                MsgDialog.Title = "Opplasting feil";
                MsgDialog.ShowAsync();
            }
            catch { }
        }
    }
}