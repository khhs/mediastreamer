﻿using MediaStreamer.Common;
using MediaStreamer.DataSourceClasses;
using MediaStreamer.FtpManagers;
using MediaStreamer.Objects;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Windows.Media;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace MediaStreamer.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MusicPage : Page
    {
        private ObservableDictionary SongsListView = new ObservableDictionary();
        public ObservableDictionary MusicViewModel { get { return this.SongsListView; } }
        private MusicDataSource MusDataSource = new MusicDataSource();
        private MediaStreamerPageHolder FramePageHolder { get{ return ((Frame)Window.Current.Content).Content as MediaStreamerPageHolder; } }
        private MediaElement MediaPlayer;
        private MediaDownloader MedDownloader = new MediaDownloader();
        private MessageDialog MsgDialog = new MessageDialog("");

        /// <summary>
        /// Initializes a new instance of the <see cref="MusicPage"/> class.
        /// </summary>
        public MusicPage()
        {
            this.InitializeComponent();
            FramePageHolder.ShowSideMenu();
            SetupMusicPage();
        }

        /// <summary>
        /// Startups the music page.
        /// </summary>
        private async void StartupMusicPage()
        {
            if (MediaPlayer.Source != null)
            {
                MediaPlayer_MediaOpened(null, null);
                String SongNameOnServerAndType = MediaPlayer.Source.LocalPath.Remove(0, MediaPlayer.Source.LocalPath.LastIndexOf("/") + 1);
                MediaSong MedSong = await MusDataSource.GetSong(Convert.ToInt32(SongNameOnServerAndType.Split('.')[0]));
                SetSongPlayingText(MedSong);
                MusicList.SelectedItem = MedSong;
                if (MediaPlayer.CurrentState == MediaElementState.Paused)
                {
                    BtnPause.Visibility = Visibility.Collapsed;
                    BtnPlay.Visibility = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Setups the music player.
        /// </summary>
        private void SetupMusicPlayer(){
            MediaPlayer = FramePageHolder.MediaPlayer;
            MediaPlayer.MediaOpened += MediaPlayer_MediaOpened;
            MediaPlayer.MediaEnded += MediaPlayer_MediaEnded;
            MediaPlayer.MediaFailed += MediaPlayer_MediaFailed;
            MusicVolume.Value = MediaPlayer.Volume * 100;
            SetupMusicIcons();
        }

        /// <summary>
        /// Setups the music page.
        /// </summary>
        private async void SetupMusicPage()
        {
            this.MusicViewModel["SongItems"] = await MusDataSource.GetSongs();
            ListViewLoader.IsActive = false;
            MusicList.SelectedIndex = -1;
            MusicList.SelectionChanged += MusicList_SelectionChanged;
            SetupMusicPlayer();
            StartupMusicPage();
        }

        /// <summary>
        /// Setups the music icons foreground color.
        /// </summary>
        private void SetupMusicIcons()
        {
            if(FramePageHolder.IsLooping)
                MusicIconReapeatOne.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 150, 255));
            else
                MusicIconReapeatOne.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

            if (FramePageHolder.IsShuffling)
                MusicIconShuffle.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 150, 255));
            else
                MusicIconShuffle.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
        }

        /// <summary>
        /// Sets the song playing text.
        /// </summary>
        /// <param name="medSong">The med song.</param>
        private void SetSongPlayingText(MediaSong medSong)
        {
            if (String.IsNullOrEmpty(medSong.Artist))
                TxtSongPlaying.Text = medSong.SongName;
            else
            {
                TxtSongPlaying.Text = medSong.Artist + " - " + medSong.SongName;
            }
        }

        /// <summary>
        /// Handles the SelectionChanged event of the MusicList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void MusicList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MediaSong MedSong = (MediaSong)((ListView)sender).SelectedItem;
            if (MedSong == null)
                return;
            if (MediaPlayer.Source != new Uri(MedSong.SongPath))
            {
                PlaySong(MedSong);
            }
        }

        /// <summary>
        /// Plays the song.
        /// </summary>
        /// <param name="medSong">The med song.</param>
        private async void PlaySong(MediaSong medSong)
        {
            MediaPlayer.Source = new Uri(medSong.SongPath);
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                SystemMediaTransportControls systemControls = SystemMediaTransportControls.GetForCurrentView();
                systemControls.IsPlayEnabled = true;
                systemControls.IsPauseEnabled = true;
                systemControls.IsStopEnabled = true;
                systemControls.IsEnabled = true;
                MediaPlayer.Play();
                BindTimeLineToMediaPlayer();
            });
            ResetMediaPlayer();
            SetSongPlayingText(medSong);
        }

        /// <summary>
        /// Handles the Click event of the NextSong control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void NextSong_Click(object sender, RoutedEventArgs e)
        {
            if(MusicList.Items.Count > 0)
                NextSong();
        }

        /// <summary>
        /// Handles the Click event of the PreviousSong control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PreviousSong_Click(object sender, RoutedEventArgs e)
        {
            if (FramePageHolder.IsShuffling)
            {
                Random RandomInt = new Random();
                MusicList.SelectedIndex = RandomInt.Next(0, MusicList.Items.Count);
            }
            else
            {
                if (MusicList.SelectedIndex > 0)
                {
                    MusicList.SelectedIndex--;
                }
                else
                {
                    MusicList.SelectedIndex = MusicList.Items.Count-1;
                }
            }
            PlaySong((MediaSong)MusicList.SelectedItem);
        }

        /// <summary>
        /// Nexts the song.
        /// </summary>
        private void NextSong()
        {
            if (FramePageHolder.IsShuffling)
            {
                Random RandomInt = new Random();
                MusicList.SelectedIndex = RandomInt.Next(0, MusicList.Items.Count);
            }
            else
            {
                if (MusicList.SelectedIndex < MusicList.Items.Count-1)
                {
                    MusicList.SelectedIndex++;
                }
                else
                {
                    MusicList.SelectedIndex = 0;
                }
            }
            PlaySong((MediaSong)MusicList.SelectedItem);
        }

        /// <summary>
        /// Handles the MediaEnded event of the MediaPlayer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (FramePageHolder.IsLooping)
            {
                MediaPlayer.Position = TimeSpan.Zero;
                BindTimeLineToMediaPlayer();
                MediaPlayer.Play();
            }
            else
            {
                ResetMediaPlayer();
                NextSong();
            }
            SetStartPauseBtnVisibilty(false);
        }

        /// <summary>
        /// Handles the MediaOpened event of the MediaPlayer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MediaPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            SetStartPauseBtnVisibilty(true);
            MusicDownloadLine.Value = 0;
            SongCounter.Text = "00:00";
            SongFullTime.Text = MediaPlayer.NaturalDuration.TimeSpan.Minutes.ToString("00") + ":" + MediaPlayer.NaturalDuration.TimeSpan.Seconds.ToString("00");
            BindTimeLineToMediaPlayer();
        }

        /// <summary>
        /// Handles the MediaFailed event of the MediaPlayer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void MediaPlayer_MediaFailed(object sender, RoutedEventArgs e)
        {
            MediaPlayer.Stop();
            MediaPlayer.Source = null;
            if (MediaPlayer.CurrentState == MediaElementState.Closed)
            {
                SetStartPauseBtnVisibilty(false);
            }
            ShowMessageDialog("Klarte ikke spille av sang");
            ResetMediaPlayer();
        }

        /// <summary>
        /// Handles the ValueChanged event of the MusicTimeLine control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs"/> instance containing the event data.</param>
        private void MusicTimeLine_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (MusicTimeLine.Value < MediaPlayer.Position.TotalSeconds - 0.01 || MusicTimeLine.Value > MediaPlayer.Position.TotalSeconds + 0.01)
            {
                if (MusicTimeLine.Value < MediaPlayer.Position.TotalSeconds - 1 || MusicTimeLine.Value > MediaPlayer.Position.TotalSeconds + 1)
                    MediaPlayer.Position = new TimeSpan(0, 0, 0, ((int)MusicTimeLine.Value), 0);
                BindTimeLineToMediaPlayer();
            }
            SongCounter.Text = MediaPlayer.Position.Minutes.ToString("00") + ":" + MediaPlayer.Position.Seconds.ToString("00");
        }

        /// <summary>
        /// Binds the time line to media player.
        /// </summary>
        private void BindTimeLineToMediaPlayer()
        {
            SetMusicTimeLineBinding();
            MusicTimeLine.ValueChanged += MusicTimeLine_ValueChanged;

            SetMusicDownloadLineBinding();
        }

        /// <summary>
        /// Sets the music download line binding.
        /// </summary>
        private void SetMusicDownloadLineBinding()
        {
            MusicDownloadLine.DataContext = MediaPlayer;
            MusicDownloadLine.SetBinding(ProgressBar.ValueProperty, new Binding()
            {
                Path = new PropertyPath("DownloadProgress")
            });
        }

        /// <summary>
        /// Sets the music time line binding.
        /// </summary>
        private void SetMusicTimeLineBinding()
        {
            MusicTimeLine.DataContext = MediaPlayer;
            MusicTimeLine.SetBinding(Slider.ValueProperty, new Binding()
            {
                Path = new PropertyPath("Position.TotalSeconds")
            });
            MusicTimeLine.Maximum = MediaPlayer.NaturalDuration.TimeSpan.TotalSeconds;
        }

        /// <summary>
        /// Handles the Click event of the PlayBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PlayBtn_Click(object sender, RoutedEventArgs e)
        {
            if(MediaPlayer.CurrentState == MediaElementState.Paused)
                SetStartPauseBtnVisibilty(true);
            if (MediaPlayer.Source != null)
            {
                MediaPlayer.Play();
                if (MusicTimeLine.Value == 0)
                    StartupMusicPage();
            }
            else if (MusicList.Items.Count > 0)
            {
                MusicList.SelectedIndex = 0;
            }
        }
        /// <summary>
        /// Handles the Click event of the PauseBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PauseBtn_Click(object sender, RoutedEventArgs e)
        {
            SetStartPauseBtnVisibilty(false);
            MediaPlayer.Pause();
        }

        /// <summary>
        /// Sets the start and pause BTN visibilty.
        /// </summary>
        /// <param name="StartOfSong">if set to <c>true</c> [start of song].</param>
        private void SetStartPauseBtnVisibilty(Boolean StartOfSong)
        {
            if (StartOfSong == true)
            {
                BtnPlay.Visibility = Visibility.Collapsed;
                BtnPause.Visibility = Visibility.Visible;
            }
            else
            {
                BtnPlay.Visibility = Visibility.Visible;
                BtnPause.Visibility = Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Resets the media player.
        /// </summary>
        private void ResetMediaPlayer()
        {
            SongCounter.Text = "00:00";
            SongFullTime.Text = "00:00";
            MusicDownloadLine.Value = 0;
            MusicTimeLine.Value = 0;
            MusicTimeLine.Maximum = 1;
            TxtSongPlaying.Text = "";
        }

        /// <summary>
        /// Handles the PointerEntered event of the SymbolIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private void SymbolIcon_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SymbolIcon MusicIcon = (SymbolIcon)sender;
            MusicIcon.Opacity = 1;
        }

        /// <summary>
        /// Handles the PointerExited event of the SymbolIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private void SymbolIcon_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SymbolIcon MusicIcon = (SymbolIcon)sender;
            MusicIcon.Opacity = 0.5;
        }

        /// <summary>
        /// Handles the PointerPressed event of the SymbolIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private void SymbolIcon_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SymbolIcon MusicIcon = (SymbolIcon)sender;
            if(MusicIcon == MusicIconReapeatOne)
                SetMediaPlayerLoop();
            else
                SetMediaPlayerShuffle();
        }

        /// <summary>
        /// Sets the media player loop.
        /// </summary>
        private void SetMediaPlayerLoop()
        {
            if (FramePageHolder.IsLooping)
            {
                FramePageHolder.IsLooping = false;
                MusicIconReapeatOne.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            }
            else
            {
                FramePageHolder.IsLooping = true;
                MusicIconReapeatOne.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 150, 255));
            }
        }

        /// <summary>
        /// Sets the media player shuffle.
        /// </summary>
        private void SetMediaPlayerShuffle()
        {
            if (FramePageHolder.IsShuffling)
            {
                FramePageHolder.IsShuffling = false;
                MusicIconShuffle.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            }
            else
            {
                FramePageHolder.IsShuffling = true;
                MusicIconShuffle.Foreground = new SolidColorBrush(Color.FromArgb(255, 150, 150, 255));
            }
        }

        /// <summary>
        /// Handles the ValueChanged event of the MusicVolume control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs"/> instance containing the event data.</param>
        private void MusicVolume_ValueChanged(object sender, Windows.UI.Xaml.Controls.Primitives.RangeBaseValueChangedEventArgs e)
        {
            if (MediaPlayer == null)
                return;
            MediaPlayer.Volume = MusicVolume.Value/100;
        }

        /// <summary>
        /// Handles the PointerEntered event of the MusicListViewItemSymbolIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private void MusicListViewItemSymbolIcon_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SymbolIcon MusicListViewItemIcon = (SymbolIcon)sender;
            MusicListViewItemIcon.Opacity = 1;
        }

        /// <summary>
        /// Handles the PointerExited event of the MusicListViewItemSymbolIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private void MusicListViewItemSymbolIcon_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SymbolIcon MusicIcon = (SymbolIcon)sender;
            MusicIcon.Opacity = 0.5;
        }

        /// <summary>
        /// Handles the PointerPressed event of the MusicListViewItemSymbolIconDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private async void MusicListViewItemSymbolIconDelete_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            SymbolIcon SymbIcnDelete = ((SymbolIcon)sender);
            SymbIcnDelete.PointerPressed -= MusicListViewItemSymbolIconDelete_PointerPressed;
            int SongId = Convert.ToInt32(SymbIcnDelete.DataContext);
            await MusDataSource.DeleteSong(SongId);
        }

        /// <summary>
        /// Handles the PointerPressed event of the MusicListViewItemSymbolIconDownload control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.PointerRoutedEventArgs"/> instance containing the event data.</param>
        private async void MusicListViewItemSymbolIconDownload_PointerPressed(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            e.Handled = true;
            SymbolIcon SymbIcnDownload = ((SymbolIcon)sender);
            ProgressRing PrgRingDownloader = (ProgressRing)((Grid)SymbIcnDownload.Parent).Children[5];
            SymbIcnDownload.Visibility = Visibility.Collapsed;
            PrgRingDownloader.IsActive = true;
            if (!await MedDownloader.DownloadSongFromServer(await MusDataSource.GetSong(Convert.ToInt32(SymbIcnDownload.DataContext))))
            {
                ShowMessageDialog("Feil under nedlasting av fil. Sjekk internet tilkobling");
            }
            PrgRingDownloader.IsActive = false;
            SymbIcnDownload.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Handles the RightTapped event of the MusicList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Windows.UI.Xaml.Input.RightTappedRoutedEventArgs"/> instance containing the event data.</param>
        private void MusicList_RightTapped(object sender, Windows.UI.Xaml.Input.RightTappedRoutedEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// Shows the message dialog. This is put in a try catch because the application crashes when it is trying to show the
        /// messagedialog the second time and this is a workaround.
        /// </summary>
        /// <param name="content">The content.</param>
        private void ShowMessageDialog(String content)
        {
            try
            {
                MsgDialog.Content = content;
                MsgDialog.ShowAsync();
            }
            catch { }
        }
    }
}
