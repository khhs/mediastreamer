﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MediaStreamer.DataSourceClasses
{
    class VideoDataSource
    {
        private ObservableCollection<MediaVideo> Videos = new ObservableCollection<MediaVideo>();
        private Uri ServiceUrl;
        private HttpClient ServiceClient;
        private ServerCredential Credentials = new ServerCredential();

        public VideoDataSource()
        {
            MediaStreamerPageHolder FramePageHolder = ((Frame)Window.Current.Content).Content as MediaStreamerPageHolder;
            ServiceClient = FramePageHolder.ServiceClient;
            ServiceUrl = FramePageHolder.ServiceUrl;
        }

        public async Task<Boolean> AddVideo(MediaVideo medVideo)
        {
            HttpResponseMessage Response = await ServiceClient.PostAsync("Video", CreateStringContentFromMediaVideo(medVideo));

            if (Response.IsSuccessStatusCode)
            {
                MediaVideo MedVideo = await CreateMediaVideoFromResponseMessage(Response);
                Videos.Add(MedVideo);
                return true;
            }
            return false;
        }

        public async Task<int> GetLastAddedVideoId()
        {
            await GetVideos();
            return Videos[Videos.Count - 1].Id;
        }

        public async Task<IEnumerable<MediaVideo>> GetVideos()
        {
            Videos = new ObservableCollection<MediaVideo>();
            try
            {
                HttpResponseMessage Response = await ServiceClient.GetAsync(ServiceUrl + "Video");
                if (Response.IsSuccessStatusCode)
                {
                    String ResponseContent = await Response.Content.ReadAsStringAsync();
                    CreateVideoArrayOfJsonObject(JsonArray.Parse(ResponseContent));
                }
            }
            catch
            {
                MessageDialog MsgDialog = new MessageDialog("Feil ved henting av videoer! Sjekk internet tilkobling");
                MsgDialog.ShowAsync();
            }
            return Videos;
        }

        public StringContent CreateStringContentFromMediaVideo(MediaVideo medVideo)
        {
            var JsonSerializer = new DataContractJsonSerializer(typeof(MediaVideo));
            var ContentStreamReader = new MemoryStream();
            JsonSerializer.WriteObject(ContentStreamReader, medVideo);
            ContentStreamReader.Position = 0;
            return new StringContent(new StreamReader(ContentStreamReader).ReadToEnd(), System.Text.Encoding.UTF8, "application/json");
        }

        public async Task<MediaVideo> CreateMediaVideoFromResponseMessage(HttpResponseMessage response)
        {
            JsonObject JsonResponseObject = JsonObject.Parse(await response.Content.ReadAsStringAsync()).GetObject();
            MediaVideo VideoObject = new MediaVideo(Convert.ToInt32(JsonResponseObject["Id"].GetNumber()), JsonResponseObject["VideoName"].GetString(),
                JsonResponseObject["FileType"].GetString());
            VideoObject.VideoPath = new Uri(Credentials.VideoFilesPath + VideoObject.Id + VideoObject.FileType).AbsoluteUri;
            return VideoObject;
        }


        private void CreateVideoArrayOfJsonObject(JsonArray jsonVideoArray)
        {
            foreach (JsonValue JsonVideo in jsonVideoArray)
            {
                JsonObject VideoObject = JsonVideo.GetObject();
                SetupMediaVideoFromJsonObject(VideoObject);
            }
        }

        private void SetupMediaVideoFromJsonObject(JsonObject videoObject)
        {
            MediaVideo MedVideo = new MediaVideo(Convert.ToInt32(videoObject["Id"].GetNumber()), videoObject["VideoName"].GetString(),
                    videoObject["FileType"].GetString());
            MedVideo.VideoPath = new Uri(Credentials.VideoFilesPath + MedVideo.Id + MedVideo.FileType).AbsoluteUri;
            Videos.Add(MedVideo);
        }
    }
}
