﻿using MediaStreamer.Objects;
using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MediaStreamer.DataSourceClasses
{
    class ImageDataSource
    {
        private ObservableCollection<MediaImage> Images = new ObservableCollection<MediaImage>();
        private Uri ServiceUrl;
        private HttpClient ServiceClient;
        private ServerCredential Credentials = new ServerCredential();

        public ImageDataSource()
        {
            MediaStreamerPageHolder FramePageHolder = ((Frame)Window.Current.Content).Content as MediaStreamerPageHolder;
            ServiceClient = FramePageHolder.ServiceClient;
            ServiceUrl = FramePageHolder.ServiceUrl;
        }

        public async Task<MediaImage> GetImage(int id)
        {
            await GetImages();

            var matches = Images.Where(image => image.Id == id);
            if (matches.Count() == 1)
                return matches.First();
            return null;
        }

        public async Task<Boolean> AddImage(MediaImage medImage)
        {
            HttpResponseMessage Response = await ServiceClient.PostAsync("Image", CreateStringContentFromMediaImage(medImage));

            if (Response.IsSuccessStatusCode)
            {
                MediaImage MedImage = await CreateMediaImageFromResponseMessage(Response);
                Images.Add(MedImage);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task UpdateImage(MediaImage medImage)
        {
            var ImageContent = CreateStringContentFromMediaImage(medImage);
            HttpResponseMessage Response = await ServiceClient.PutAsync("Image/" + medImage.Id, ImageContent);
            if (!Response.IsSuccessStatusCode)
                return;
            var ImageItem = Images.First(i => i.Id == medImage.Id);
            if (ImageItem != null)
            {
                ImageItem.ImageName = medImage.ImageName;
            }
        }

        public async Task DeleteImage(int id)
        {
            var MedImage = Images.First(i => i.Id == id);
            if (!await DeleteImageFromServer(MedImage))
                return;
            HttpResponseMessage Response = await ServiceClient.DeleteAsync("Image/" + id);
            if (Response.IsSuccessStatusCode)
            {
                Images.Remove(MedImage);
            }
        }

        private async Task<Boolean> DeleteImageFromServer(MediaImage medImage)
        {
            try
            {
                WebRequest Request = WebRequest.Create(Credentials.ImageServerFolderPath + medImage.Id + medImage.FileType);
                Request.Credentials = Credentials;
                Request.Method = "DELE";
                await Request.GetResponseAsync();
                return true;
            }
            catch{ }
            return false;
        }

        public async Task<int> GetLastAddedImageId()
        {
            await GetImages();
            return Images[Images.Count - 1].Id;
        }

        public async Task<IEnumerable<MediaImage>> GetImages()
        {
            Images = new ObservableCollection<MediaImage>();
            try
            {
                HttpResponseMessage Response = await ServiceClient.GetAsync(ServiceUrl + "Image");
                if (Response.IsSuccessStatusCode)
                {
                    String ResponseContent = await Response.Content.ReadAsStringAsync();
                    CreateImageArrayOfJsonObject(JsonArray.Parse(ResponseContent));
                }
            }
            catch
            {
                MessageDialog msgDialog = new MessageDialog("Feil ved henting av bilder! Sjekk internet tilkobling");
                msgDialog.ShowAsync();
            }
            return Images;
        }

        public StringContent CreateStringContentFromMediaImage(MediaImage medImage)
        {
            var JsonSerializer = new DataContractJsonSerializer(typeof(MediaImage));

            var ContentStreamReader = new MemoryStream();
            JsonSerializer.WriteObject(ContentStreamReader, medImage);
            ContentStreamReader.Position = 0;
            return new StringContent(new StreamReader(ContentStreamReader).ReadToEnd(), System.Text.Encoding.UTF8, "application/json");
        }
        public async Task<MediaImage> CreateMediaImageFromResponseMessage(HttpResponseMessage response)
        {
            JsonObject JsonResponseObject = JsonObject.Parse(await response.Content.ReadAsStringAsync()).GetObject();
            MediaImage ImageObject = new MediaImage(Convert.ToInt32(JsonResponseObject["Id"].GetNumber()), JsonResponseObject["ImageName"].GetString(), JsonResponseObject["FileType"].GetString());
            ImageObject.ImagePath = new Uri(Credentials.ImageFilesPath + ImageObject.Id + ImageObject.FileType).AbsoluteUri;
            return ImageObject;
        }

        /// <summary>
        /// Creates array of MediaImage object of a json object.
        /// </summary>
        /// <param name="jsonImageArray">The json image array.</param>
        /// <returns></returns>
        private void CreateImageArrayOfJsonObject(JsonArray jsonImageArray)
        {
            foreach (JsonValue ImageValue in jsonImageArray)
            {
                JsonObject ImageObject = ImageValue.GetObject();
                SetupMediaImageFromJsonObject(ImageObject);
            }
        }

        private void SetupMediaImageFromJsonObject(JsonObject imageObject)
        {
            MediaImage MedImage = new MediaImage(Convert.ToInt32(imageObject["Id"].GetNumber()), imageObject["ImageName"].GetString(), imageObject["FileType"].GetString());
            MedImage.ImagePath = new Uri(Credentials.ImageFilesPath + MedImage.Id + MedImage.FileType).AbsoluteUri;
            Images.Add(MedImage);
        }
    }
}
