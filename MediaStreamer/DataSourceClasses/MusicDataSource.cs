﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MediaStreamer.DataSourceClasses
{
    class MusicDataSource
    {
        private ObservableCollection<MediaSong> Songs = new ObservableCollection<MediaSong>();
        private ServerCredential Credentials = new ServerCredential();
        private Uri ServiceUrl;
        private HttpClient ServiceClient;


        public MusicDataSource()
        {
            MediaStreamerPageHolder FramePageHolder = ((Frame)Window.Current.Content).Content as MediaStreamerPageHolder;
            ServiceClient = FramePageHolder.ServiceClient;
            ServiceUrl = FramePageHolder.ServiceUrl;
        }

        public async Task<MediaSong> GetSong(int id)
        {
            await GetSongs();
            var matches = Songs.Where(song => song.Id == id);
            if (matches.Count() == 1)
                return matches.First();
            return null;
        }

        public async Task<Boolean> AddSong(MediaSong medSong)
        {
            HttpResponseMessage Response = await ServiceClient.PostAsync("Music", CreateStringContentFromMediaSong(medSong));

            if (Response.IsSuccessStatusCode)
            {
                MediaSong MedSong = await CreateMediaSongFromResponseMessage(Response);
                Songs.Add(MedSong);
                return true;
            }
            return false;
        }

        public async Task DeleteSong(int id)
        {
            if (Songs.Count == 0)
                await GetSongs();
            var MedSong = Songs.First(s => s.Id == id);
            Songs.Remove(MedSong);
            if (!await DeleteSongFromServer(MedSong))
                return;
            HttpResponseMessage Response = await ServiceClient.DeleteAsync("Music/" + id);
        }

        private async Task<Boolean> DeleteSongFromServer(MediaSong medSong)
        {
            try
            {
                WebRequest Request = WebRequest.Create(Credentials.MusicServerFolderPath + medSong.Id + medSong.FileType);
                Request.Credentials = Credentials;
                Request.Method = "DELE";
                await Request.GetResponseAsync();
                return true;
            }
            catch (Exception webExc)
            {
                if (webExc.Message.Contains("550"))
                    return true;
            }
            return false;
        }

        public async Task<int> GetLastAddedSongId()
        {
            await GetSongs();
            return Songs[Songs.Count - 1].Id;
        }

        public async Task<IEnumerable<MediaSong>> GetSongs()
        {
            Songs = new ObservableCollection<MediaSong>();
            try
            {
                HttpResponseMessage Response = await ServiceClient.GetAsync(ServiceUrl + "Music");
                if (Response.IsSuccessStatusCode)
                {
                    String ResponseContent = await Response.Content.ReadAsStringAsync();
                    CreateSongArrayOfJsonObject(JsonArray.Parse(ResponseContent));
                }
            }
            catch
            {
                MessageDialog msgDialog = new MessageDialog("Feil ved henting av sanger! Sjekk internet tilkobling");
                msgDialog.ShowAsync();
            }
            return Songs;
        }

        public StringContent CreateStringContentFromMediaSong(MediaSong medSong)
        {
            var JsonSerializer = new DataContractJsonSerializer(typeof(MediaSong));
            var ContentStreamReader = new MemoryStream();
            JsonSerializer.WriteObject(ContentStreamReader, medSong);
            ContentStreamReader.Position = 0;
            return new StringContent(new StreamReader(ContentStreamReader).ReadToEnd(), System.Text.Encoding.UTF8, "application/json");
        }

        public async Task<MediaSong> CreateMediaSongFromResponseMessage(HttpResponseMessage response)
        {
            JsonObject JsonResponseObject = JsonObject.Parse(await response.Content.ReadAsStringAsync()).GetObject();
            MediaSong SongObject = new MediaSong(Convert.ToInt32(JsonResponseObject["Id"].GetNumber()), JsonResponseObject["SongName"].GetString(),
                JsonResponseObject["FileType"].GetString(), JsonResponseObject["Artist"].GetString(), JsonResponseObject["Album"].GetString(), JsonResponseObject["SongLength"].GetString());
            SongObject.SongPath = new Uri(Credentials.MusicFilesPath + SongObject.Id + SongObject.FileType).AbsoluteUri;
            return SongObject;
        }


        private void CreateSongArrayOfJsonObject(JsonArray jsonSongArray)
        {
            foreach (JsonValue JsonSongValue in jsonSongArray)
            {
                JsonObject JsonSongObject = JsonSongValue.GetObject();
                SetupMediaSongFromJsonObject(JsonSongObject);
            }
        }

        private void SetupMediaSongFromJsonObject(JsonObject jsonSongObject)
        {
            MediaSong SongObject = new MediaSong(Convert.ToInt32(jsonSongObject["Id"].GetNumber()), jsonSongObject["SongName"].GetString(),
                jsonSongObject["FileType"].GetString(), jsonSongObject["Artist"].GetString(), jsonSongObject["Album"].GetString(), jsonSongObject["SongLength"].GetString());
            SongObject.SongPath = new Uri(Credentials.MusicFilesPath + SongObject.Id + SongObject.FileType).AbsoluteUri;
            Songs.Add(SongObject);
        }
    }
}
