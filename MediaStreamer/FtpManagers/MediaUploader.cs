﻿using MediaStreamer.DataSourceClasses;
using MediaStreamer.NotifyPropertyChangedClasses;
using MediaStreamer.Objects;
using Model;
using System;
using System.IO;
using System.Net;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MediaStreamer.FtpManagers
{
    class MediaUploader
    {
        private Stream UploadStream;
        private long BytesUploaded;
        private UploadProgress UplProgress;
        private ServerCredential Credentials = new ServerCredential();
        private MusicDataSource MusDataSource = new MusicDataSource();
        private ImageDataSource ImgDataSource = new ImageDataSource();
        private VideoDataSource VidDataSource = new VideoDataSource();
        private MediaStreamerPageHolder FramePageHolder = ((Frame)Window.Current.Content).Content as MediaStreamerPageHolder;
        private IProgress<Object> ProgressUpdater;

        private void StartUploadingMediaFile(MediaFile medFile)
        {
            Task.Factory.StartNew(async () =>
            {
                await UploaderTaskToDo(medFile);
            });
        }

        private async Task UploaderTaskToDo(MediaFile medFile)
        {
            try
            {
                await medFile.SetupUploadBuffer();
                BytesUploaded = 0;

                foreach (Byte b in medFile.FileBytes)
                {
                    UploadStream.WriteByte(b);
                    BytesUploaded++;
                    if (BytesUploaded % 10000 == 0)
                        ProgressUpdater.Report(null);
                }
            }
            catch (Exception FtpExc)
            {
                if (FtpExc.Message.Contains("forcibly closed"))
                {
                    ProgressUpdater.Report("Disconnected");
                }
            }
            FramePageHolder.NotifyUploadProgress = null;
            await UploadStream.FlushAsync();
            ProgressUpdater.Report(null);
            UploadStream.Dispose();
        }

        private void UpdateUploaderProgress(object ReportMessage, MediaFile medFile)
        {
            UplProgress.UploaderProgress = BytesUploaded;
            if (ReportMessage == null)
                return;
            if (ReportMessage.ToString().Equals("Disconnected"))
            {
                FramePageHolder.NotifyUploadProgress = null;
                UplProgress.UploaderProgress = medFile.FileBytes.Count;
                try
                {
                    MessageDialog MsgDialog = new MessageDialog("Mistet tilkobling til server.");
                    MsgDialog.Title = "Opplasting feil";
                    MsgDialog.ShowAsync();
                }
                catch { }
            }
        }

        private async Task<Boolean> SetupUpload(String serverPath, MediaFile medFile, int id)
        {
            try
            {
                WebRequest Request = WebRequest.Create(serverPath + id + medFile.File.FileType);
                Request.Credentials = Credentials;
                Request.Method = "STOR";
                UploadStream = await Request.GetRequestStreamAsync();
                StartUploadingMediaFile(medFile);
                ProgressUpdater = new Progress<object>(ReportMessage => UpdateUploaderProgress(ReportMessage, medFile));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Boolean> UploadMediaImage(MediaImage medImage, MediaFile medFile, UploadProgress uplProgress)
        {
            UplProgress = uplProgress;
            int ImageId = await ImgDataSource.GetLastAddedImageId();
            return await SetupUpload(Credentials.ImageServerFolderPath, medFile, ImageId);
        }

        public async Task<Boolean> UploadMediaSong(MediaSong medSong, MediaFile medFile, UploadProgress uplProgress)
        {
            UplProgress = uplProgress;
            int SongId = await MusDataSource.GetLastAddedSongId();
            return await SetupUpload(Credentials.MusicServerFolderPath, medFile, SongId);
        }

        public async Task<Boolean> UploadMediaVideo(MediaVideo medVideo, MediaFile medFile, UploadProgress uplProgress)
        {
            UplProgress = uplProgress;
            int VideoId = await VidDataSource.GetLastAddedVideoId();
            return await SetupUpload(Credentials.VideoServerFolderPath, medFile, VideoId);
        }
    }
}
