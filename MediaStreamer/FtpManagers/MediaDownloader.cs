﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace MediaStreamer.FtpManagers
{
    class MediaDownloader
    {
        ServerCredential Credentials = new ServerCredential();

        private async Task<WebResponse> SetupWebRequest(String url)
        {
            WebRequest Request = WebRequest.Create(url);
            Request.Credentials = Credentials;
            Request.Method = "RETR";
            return await Request.GetResponseAsync();
        }

        public async Task<Boolean> DownloadSongFromServer(MediaSong medSong)
        {
            try
            {
                WebResponse Response = await SetupWebRequest(Credentials.MusicServerFolderPath + medSong.Id + medSong.FileType);
                Byte[] FileBuffer = await DownloadFile(Response.GetResponseStream(), await GetFileSize(medSong.SongPath));
                if(!medSong.Artist.Equals(""))
                    await CreateFile(Windows.Storage.KnownFolders.MusicLibrary, medSong.Artist + " - " + medSong.SongName + medSong.FileType, FileBuffer);
                else
                    await CreateFile(Windows.Storage.KnownFolders.MusicLibrary, medSong.SongName + medSong.FileType, FileBuffer);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Boolean> DownloadImageFromServer(MediaImage medImage)
        {
            try
            {
                WebResponse Response = await SetupWebRequest(Credentials.ImageServerFolderPath + medImage.Id + medImage.FileType);
                Byte[] FileBuffer = await DownloadFile(Response.GetResponseStream(), await GetFileSize(medImage.ImagePath));
                await CreateFile(Windows.Storage.KnownFolders.PicturesLibrary, medImage.ImageName + medImage.FileType, FileBuffer);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task<Byte[]> DownloadFile(Stream downloadStream, long fileSize)
        {
            Byte[] FileBuffer = new Byte[fileSize];
            await downloadStream.ReadAsync(FileBuffer, 0, FileBuffer.Length);
            return FileBuffer;
        }

        private async Task CreateFile(StorageFolder downloadFolder, String fileName, Byte[] fileBuffer)
        {
            StorageFile FileDownloaded = await downloadFolder.CreateFileAsync(fileName, CreationCollisionOption.GenerateUniqueName);
            Stream FileWriteStream = await FileDownloaded.OpenStreamForWriteAsync();
            await FileWriteStream.WriteAsync(fileBuffer, 0, fileBuffer.Length);
        }

        private async Task<long> GetFileSize(String fileUri)
        {
            WebRequest Request = HttpWebRequest.Create(fileUri);
            Request.Method = "HEAD";
            WebResponse Response = await Request.GetResponseAsync();
            return Response.ContentLength;
        }
    }
}
