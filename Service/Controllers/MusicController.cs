﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using DataAccess;

namespace Service.Controllers
{
    public class MusicController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET api/Music
        public IQueryable<MediaSong> GetSongs()
        {
            return db.Songs;
        }

        // GET api/Music/5
        [ResponseType(typeof(MediaSong))]
        public IHttpActionResult GetMediaSong(int id)
        {
            MediaSong mediasong = db.Songs.Find(id);
            if (mediasong == null)
            {
                return NotFound();
            }

            return Ok(mediasong);
        }

        // PUT api/Music/5
        public IHttpActionResult PutMediaSong(int id, MediaSong mediasong)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mediasong.Id)
            {
                return BadRequest();
            }

            db.Entry(mediasong).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MediaSongExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Music
        [ResponseType(typeof(MediaSong))]
        public IHttpActionResult PostMediaSong(MediaSong mediasong)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Songs.Add(mediasong);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mediasong.Id }, mediasong);
        }

        // DELETE api/Music/5
        [ResponseType(typeof(MediaSong))]
        public IHttpActionResult DeleteMediaSong(int id)
        {
            MediaSong mediasong = db.Songs.Find(id);
            if (mediasong == null)
            {
                return NotFound();
            }

            db.Songs.Remove(mediasong);
            db.SaveChanges();

            return Ok(mediasong);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MediaSongExists(int id)
        {
            return db.Songs.Count(e => e.Id == id) > 0;
        }
    }
}