﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using DataAccess;

namespace Service.Controllers
{
    public class VideoController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET api/Video
        public IQueryable<MediaVideo> GetVideos()
        {
            return db.Videos;
        }

        // GET api/Video/5
        [ResponseType(typeof(MediaVideo))]
        public IHttpActionResult GetMediaVideo(int id)
        {
            MediaVideo mediavideo = db.Videos.Find(id);
            if (mediavideo == null)
            {
                return NotFound();
            }

            return Ok(mediavideo);
        }

        // PUT api/Video/5
        public IHttpActionResult PutMediaVideo(int id, MediaVideo mediavideo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mediavideo.Id)
            {
                return BadRequest();
            }

            db.Entry(mediavideo).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MediaVideoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Video
        [ResponseType(typeof(MediaVideo))]
        public IHttpActionResult PostMediaVideo(MediaVideo mediavideo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Videos.Add(mediavideo);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mediavideo.Id }, mediavideo);
        }

        // DELETE api/Video/5
        [ResponseType(typeof(MediaVideo))]
        public IHttpActionResult DeleteMediaVideo(int id)
        {
            MediaVideo mediavideo = db.Videos.Find(id);
            if (mediavideo == null)
            {
                return NotFound();
            }

            db.Videos.Remove(mediavideo);
            db.SaveChanges();

            return Ok(mediavideo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MediaVideoExists(int id)
        {
            return db.Videos.Count(e => e.Id == id) > 0;
        }
    }
}