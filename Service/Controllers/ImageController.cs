﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Model;
using DataAccess;

namespace Service.Controllers
{
    public class ImageController : ApiController
    {
        private DatabaseContext db = new DatabaseContext();

        // GET api/Image
        public IQueryable<MediaImage> GetImages()
        {
            return db.Images;
        }

        // GET api/Image/5
        [ResponseType(typeof(MediaImage))]
        public IHttpActionResult GetMediaImage(int id)
        {
            MediaImage mediaimage = db.Images.Find(id);
            if (mediaimage == null)
            {
                return NotFound();
            }

            return Ok(mediaimage);
        }

        // PUT api/Image/5
        public IHttpActionResult PutMediaImage(int id, MediaImage mediaimage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mediaimage.Id)
            {
                return BadRequest();
            }

            db.Entry(mediaimage).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MediaImageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Image
        [ResponseType(typeof(MediaImage))]
        public IHttpActionResult PostMediaImage(MediaImage mediaimage)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Images.Add(mediaimage);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mediaimage.Id }, mediaimage);
        }

        // DELETE api/Image/5
        [ResponseType(typeof(MediaImage))]
        public IHttpActionResult DeleteMediaImage(int id)
        {
            MediaImage mediaimage = db.Images.Find(id);
            if (mediaimage == null)
            {
                return NotFound();
            }

            db.Images.Remove(mediaimage);
            db.SaveChanges();

            return Ok(mediaimage);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MediaImageExists(int id)
        {
            return db.Images.Count(e => e.Id == id) > 0;
        }
    }
}